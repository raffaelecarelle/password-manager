<?php

function getArrayFromCSV()
{
    $array = $fields = array();
    $i = 0;
    $handle = @fopen("file_ftp.csv", "r");
    if ($handle) {
        while (($row = fgetcsv($handle, 4096)) !== false) {
            if (empty($fields)) {
                $fields = $row;
                continue;
            }
            foreach ($row as $k => $value) {
                $k++;
                $valueArr = explode(';', $value);
                $array[][$k] = $valueArr;
            }
            $i++;
        }
        if (!feof($handle)) {
            echo "Error: unexpected fgets() fail\n";
        }
        fclose($handle);
    }

    return $array;
}
