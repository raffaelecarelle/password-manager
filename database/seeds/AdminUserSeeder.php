<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'Q-Web';
        $user->email = 'demo@qweb.eu';
        $user->password = bcrypt('password');

        $user->save();
    }
}
