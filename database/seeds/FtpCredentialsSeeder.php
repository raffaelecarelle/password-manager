<?php

use Illuminate\Database\Seeder;

class FtpCredentialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        require __DIR__ . "/../../csv-to-array.php";

        $array = getArrayFromCSV();

        foreach ($array as $el) {
            $project = \App\Project::where('name', $el[1][0])->first();

            if ($project != NULL && $project->count() > 0) {
                $property = \App\Property::where('project_id', $project->id)->where('name', 'FTP')->first();
                $credential = new \App\Credential();
                $credential->name = 'Username';
                $credential->value = $el[1][1];
                $credential->property_id = $property->id;
                $credential->save();

                $credential = new \App\Credential();
                $credential->name = 'Password';
                $credential->value = $el[1][2];
                $credential->property_id = $property->id;
                $credential->save();
            }
        }
    }
}
