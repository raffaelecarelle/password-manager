<?php

use Illuminate\Database\Seeder;

class PropertiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = \App\Project::all();
        foreach ($projects as $project) {
            $property = new \App\Property();
            $property->name = 'FTP';
            $property->project_id = $project->id;
            $property->save();
        }

        foreach ($projects as $project) {
            $property = new \App\Property();
            $property->name = 'MySQL';
            $property->project_id = $project->id;
            $property->save();
        }

        foreach ($projects as $project) {
            $property = new \App\Property();
            $property->name = 'Web';
            $property->project_id = $project->id;
            $property->save();
        }
    }
}
