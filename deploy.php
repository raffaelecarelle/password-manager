<?php

    namespace Deployer;

    require 'recipe/laravel.php';

    set('bin/php', function () {
        return '/opt/cpanel/ea-php70/root/usr/bin/php';
    });

    // Project name
    set('application', '');

    // Project repository
    set('repository', 'https://raffaelecarelle@bitbucket.org/raffaelecarelle/password-manager.git');

    // [Optional] Allocate tty for git clone. Default value is false.
    set('git_tty', TRUE);

    // Shared files/dirs between deploys
    add('shared_files', []);
    add('shared_dirs', []);

    // Writable dirs by web server
    add('writable_dirs', []);
    set('allow_anonymous_stats', FALSE);

    host('test.qweb.eu')
        ->user('qwebtest')
        ->port(10112)
        ->identityFile('~/.ssh/id_rsa')
        ->set('deploy_path', '~/{{application}}');

    // Tasks
    task('build', function () {
        run('cd {{release_path}} && build');
    });

    desc('Deploy your project');
    task('deploy', [
        'deploy:info',
        'deploy:prepare',
        'deploy:lock',
        'deploy:release',
        'deploy:update_code',
        'deploy:shared',
        'deploy:vendors',
        //'deploy:writable',
        'artisan:storage:link',
        'artisan:view:clear',
        'artisan:cache:clear',
        'artisan:config:cache',
        'artisan:optimize',
        'deploy:symlink',
        'deploy:unlock',
        'cleanup',
    ]);

    // [Optional] if deploy fails automatically unlock.
    after('deploy:failed', 'deploy:unlock');

    // Migrate database before symlink new release.
    //before('deploy:symlink', 'artisan:migrate');
    //before('deploy:symlink', 'artisan:db:seed');

